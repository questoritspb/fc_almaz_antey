<!doctype html>
<html lang="ru" class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Официальный сайт футбольного клуба «Алмаз-Антей» Санкт-Петербург</title>
		<meta name="og:description" content="Официальный сайт футбольного клуба «Алмаз-Антей» Санкт-Петербург">
		<link rel="canonical" href="http://fc-almaz-antey.ru/">
		<meta property="og:locale" content="ru_RU">
		<meta property="og:url" content="http://fc-almaz-antey.ru/">
		<meta property="og:type" content="website">
		<meta property="og:site_name" content="fc-almaz-antey.ru">
		<meta property="og:title" content="Официальный сайт футбольного клуба «Алмаз-Антей» Санкт-Петербург">
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:image" content="img/tw-card.jpg">
		<meta name="twitter:title" content="Официальный сайт футбольного клуба «Алмаз-Антей» Санкт-Петербург">
		<meta name="twitter:description" content="Официальный сайт футбольного клуба «Алмаз-Антей» Санкт-Петербург">
		<link rel="icon" href="img/favicon.svg">
		<link rel="mask-icon" href="img/mask-icon.svg" color="#2a3795">
		<link rel="icon" href="img/favicon-32x32.png" sizes="32x32">
		<link rel="icon" href="img/favicon-64x64.png" sizes="64x64">
		<link rel="icon" href="img/favicon-192x192.png" sizes="192x192">
		<link rel="icon" href="img/favicon-512x512.png" sizes="512x512">
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">
		<meta name="msapplication-TileImage" content="img/favicon-270x270.png">
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link rel="stylesheet" href="css/main.css">
		<!-- livereload script / delete before deploy! -->
		<!-- <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script> -->
	</head>
	<body>
		<!-- WRAP -->
		<main class="wrap-site dis-fl fl-colnw">
			<!-- ****** -->
			<!-- HEADER -->
			<header>
				<div class="dis-fl fl-colnw jsc-spbet alit-ct">
					<div class="top dis-fl fl-rwwr jst-flst alit-flst">
						<a href="index.php" class="logo">
							<img src="img/logo.svg" alt="fc-almaz-antey.ru">
						</a>
						<p class="desc"><span>Официальный сайт футбольного клуба</span><span class="name">«Алмаз-Антей» </span><span class="city">Санкт-Петербург</span></p>
					</div>
					<nav class="site-nav">
						<div class="container dis-fl fl-rwnw jsc-flst alit-ct">
							<ul class="main-menu dis-fl fl-rwwr jsc-flst alit-flst">
								<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Новости</span></a></li>
								<li>
									<a href="#" class="dropdown dis-fl fl-colnw jsc-ct alit-ct"><span>Клуб</span><i class="fas fa-caret-down"></i></a>
									<ul class="dropdown">
										<div class="container dis-fl fl-rwwr jsc-flst alit-flst">
											<li><a href="history.html" class="dis-fl fl-colnw jsc-ct alit-ct"><span>История клуба</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Руководство</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Концепция клуба</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Философия клуба</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Спонсоры и партнеры</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Кубки</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Стадион</span></a></li>
										</div>
									</ul>
								</li>
								<li>
									<a href="#" class="dropdown dis-fl fl-colnw jsc-ct alit-ct"><span>Команда</span><i class="fas fa-caret-down"></i></a>
									<ul class="dropdown">
										<div class="container dis-fl fl-rwwr jsc-flst alit-flst">
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>История клуба</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Руководство</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Концепция клуба</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Философия клуба</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Спонсоры и партнеры</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Кубки</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Стадион</span></a></li>
										</div>
									</ul>
								</li>
								<li>
									<a href="#" class="dropdown dis-fl fl-colnw jsc-ct alit-ct"><span>Матчи</span><i class="fas fa-caret-down"></i></a>
									<ul class="dropdown">
										<div class="container dis-fl fl-rwwr jsc-flst alit-flst">
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>История клуба</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Руководство</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Концепция клуба</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Философия клуба</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Спонсоры и партнеры</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Кубки</span></a></li>
											<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Стадион</span></a></li>
										</div>
									</ul>
								</li>
								<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Медиа</span></a></li>
								<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Магазин</span></a></li>
								<li><a href="#" class="dis-fl fl-colnw jsc-ct alit-ct"><span>Контакты</span></a></li>
							</ul>
							<ul class="social">
								<li><a href="#">
									<i class="fab fa-vk"></i>
								</a></li>
								<li><a href="#">
									<i class="fab fa-youtube"></i>
								</a></li>
								<li><a href="#">
									<i class="fab fa-instagram"></i>
								</a></li>
							</ul>
						</div>
					</nav>
					<div class="menu-toggle">
						<div class="hamburger"></div>
					</div>
				</div>
			</header>
			<!-- ******* -->
			<!-- CONTENT -->
			<!-- ********* -->
			<!-- 1ST BLOCK -->
			<section class="sect sect-main">
				<div class="content dis-grid">
					<!-- slider 1 -->
					<div class="slider" id="slider-main">
						<div class="arrows dis-fl fl-rwnw jsc-spbet alit-ct" id="arrows"></div>
						<div class="carousel-cell">
							<figure class="main-slide">
								<a href="#">
									<div class="img">
										<img src="img/img-main.jpg" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-flend">
										<h2>МАКСИМ КРЫЧАНОВ: «Наши тренеры работают с горящими глазами, у них есть желание совершенствоваться»</h2>
										<p class="desc">О том, как закончилась первая половина Первенства СПб, о блистательном результате молодежной команды и о планах клуба не летний период рассказывает старший тренер ФК «Алмаз-Антей» Максим Андреевич Крычанов.</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="main-slide">
								<a href="#">
									<div class="img">
										<img src="img/img-main.jpg" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-flend">
										<h2>МАКСИМ КРЫЧАНОВ: «Наши тренеры работают с горящими глазами, у них есть желание совершенствоваться»</h2>
										<p class="desc">О том, как закончилась первая половина Первенства СПб, о блистательном результате молодежной команды и о планах клуба не летний период рассказывает старший тренер ФК «Алмаз-Антей» Максим Андреевич Крычанов.</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="main-slide">
								<a href="#">
									<div class="img">
										<img src="img/img-main.jpg" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-flend">
										<h2>МАКСИМ КРЫЧАНОВ: «Наши тренеры работают с горящими глазами, у них есть желание совершенствоваться»</h2>
										<p class="desc">О том, как закончилась первая половина Первенства СПб, о блистательном результате молодежной команды и о планах клуба не летний период рассказывает старший тренер ФК «Алмаз-Антей» Максим Андреевич Крычанов.</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="main-slide">
								<a href="#">
									<div class="img">
										<img src="img/img-main.jpg" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-flend">
										<h2>МАКСИМ КРЫЧАНОВ: «Наши тренеры работают с горящими глазами, у них есть желание совершенствоваться»</h2>
										<p class="desc">О том, как закончилась первая половина Первенства СПб, о блистательном результате молодежной команды и о планах клуба не летний период рассказывает старший тренер ФК «Алмаз-Антей» Максим Андреевич Крычанов.</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="main-slide">
								<a href="#">
									<div class="img">
										<img src="img/img-main.jpg" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-flend">
										<h2>МАКСИМ КРЫЧАНОВ: «Наши тренеры работают с горящими глазами, у них есть желание совершенствоваться»</h2>
										<p class="desc">О том, как закончилась первая половина Первенства СПб, о блистательном результате молодежной команды и о планах клуба не летний период рассказывает старший тренер ФК «Алмаз-Антей» Максим Андреевич Крычанов.</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="main-slide">
								<a href="#">
									<div class="img">
										<img src="img/img-main.jpg" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-flend">
										<h2>МАКСИМ КРЫЧАНОВ: «Наши тренеры работают с горящими глазами, у них есть желание совершенствоваться»</h2>
										<p class="desc">О том, как закончилась первая половина Первенства СПб, о блистательном результате молодежной команды и о планах клуба не летний период рассказывает старший тренер ФК «Алмаз-Антей» Максим Андреевич Крычанов.</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="main-slide">
								<a href="#">
									<div class="img">
										<img src="img/img-main.jpg" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-flend">
										<h2>МАКСИМ КРЫЧАНОВ: «Наши тренеры работают с горящими глазами, у них есть желание совершенствоваться»</h2>
										<p class="desc">О том, как закончилась первая половина Первенства СПб, о блистательном результате молодежной команды и о планах клуба не летний период рассказывает старший тренер ФК «Алмаз-Антей» Максим Андреевич Крычанов.</p>
									</figcaption>
								</a>
							</figure>
						</div>
					</div>
					<!-- slider 2 -->
					<div class="slider" id="slider-match_center">
						<div class="arrows dis-fl fl-rwnw jsc-spbet alit-ct" id="arrows_2"></div>
						<div class="carousel-cell">
							<div class="cont dis-fl fl-colnw jsc-spbet alit-ct">
								<p class="subtitle">Первенство СПб 2019 год U-21</p>
								<p class="date">20 октября, вс   14:00</p>
								<div class="hor dis-grid">
									<div class="lside">
										<p class="team">Алмаз-Антей</p>
										<img src="img/logo.svg" alt="" class="logo">
									</div>
									<p class="score">3<span>:</span>1</p>
									<div class="rside">
										<p class="team">СШОР Зенит</p>
										<img src="img/zenit-logo.svg" alt="" class="logo">
									</div>
								</div>
								<a href="#" class="btn btn-lt">Матч-центр</a>
							</div>
						</div>
						<div class="carousel-cell">
							<div class="cont dis-fl fl-colnw jsc-spbet alit-ct">
								<p class="subtitle">Первенство СПб 2019 год U-21</p>
								<p class="date">20 октября, вс   14:00</p>
								<div class="hor dis-grid">
									<div class="lside">
										<p class="team">Алмаз-Антей</p>
										<img src="img/logo.svg" alt="" class="logo">
									</div>
									<p class="score">3<span>:</span>1</p>
									<div class="rside">
										<p class="team">СШОР Зенит</p>
										<img src="img/zenit-logo.svg" alt="" class="logo">
									</div>
								</div>
								<a href="#" class="btn btn-lt">Матч-центр</a>
							</div>
						</div>
					</div>
					<!-- slider 3 -->
					<div class="slider" id="slider-calendar">
						<p class="subtitle">Следующий матч</p>
						<div class="arrows dis-fl fl-rwnw jsc-spbet alit-flst" id="arrows_3"></div>
						<div class="carousel-cell">
							<div class="cont dis-fl fl-colnw jsc-spbet alit-ct">
								<p class="date">31.10.2019</p>
								<div class="hor dis-grid">
									<div class="lside">
										<img src="img/logo.svg" alt="" class="logo">
										<p class="team">Алмаз-Антей</p>
									</div>
									<div class="center">
										<p class="time">18:00</p>
										<p class="perv">Зимнее первенство 2018/19</p>
									</div>
									<div class="rside">
										<img src="img/vo_zvezda-logo.png" alt="" class="logo">
										<p class="team">СШ №2 ВО Звезда</p>
									</div>
								</div>
							</div>
						</div>
						<div class="carousel-cell">
							<div class="cont dis-fl fl-colnw jsc-spbet alit-ct">
								<p class="date">31.10.2019</p>
								<div class="hor dis-grid">
									<div class="lside">
										<img src="img/logo.svg" alt="" class="logo">
										<p class="team">Алмаз-Антей</p>
									</div>
									<div class="center">
										<p class="time">18:00</p>
										<p class="perv">Зимнее первенство 2018/19</p>
									</div>
									<div class="rside">
										<img src="img/vo_zvezda-logo.png" alt="" class="logo">
										<p class="team">СШ №2 ВО Звезда</p>
									</div>
								</div>
							</div>
						</div>
						<a href="#" class="btn btn-drk">календарь матчей</a>
					</div>
					<div class="table dis-fl fl-colnw jsc-spbet alit-ct">
						<div class="input">
							<div class="custom-select">
								<select class="select" id="perv-sort" name="perv-sort">
									<option value="Первенство СПб 2019 год - U16">Первенство СПб 2019 год - U16</option>
									<option value="Первенство СПб 2019 год - U16">Первенство СПб 2019 год - U16 2</option>
									<option value="Первенство СПб 2019 год - U16">Первенство СПб 2019 год - U16 3</option>
									<option value="Первенство СПб 2019 год - U16">Первенство СПб 2019 год - U16 4</option>
								</select>
							</div>
						</div>
						<table class="tbl tbl-main">
							<thead>
								<tr>
									<th></th>
									<th>Команда</th>
									<th>И</th>
									<th>В</th>
									<th>Н</th>
									<th>П</th>
									<th>М</th>
									<th>О</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>СШОР Зенит</td>
									<td>12</td>
									<td>16</td>
									<td>12</td>
									<td>16</td>
									<td>95 - 5</td>
									<td>16</td>
								</tr>
								<tr class="active">
									<td>2</td>
									<td>Алмаз -Антей</td>
									<td>12</td>
									<td>16</td>
									<td>12</td>
									<td>16</td>
									<td>95 - 5</td>
									<td>16</td>
								</tr>
								<tr>
									<td>3</td>
									<td>ФК Зенит</td>
									<td>12</td>
									<td>16</td>
									<td>12</td>
									<td>16</td>
									<td>95 - 5</td>
									<td>16</td>
								</tr>
								<tr>
									<td>4</td>
									<td>ФК Зенит</td>
									<td>12</td>
									<td>16</td>
									<td>12</td>
									<td>16</td>
									<td>95 - 5</td>
									<td>16</td>
								</tr>
								<tr>
									<td>5</td>
									<td>Коломяги (ШИ #357)</td>
									<td>12</td>
									<td>16</td>
									<td>12</td>
									<td>16</td>
									<td>95 - 5</td>
									<td>16</td>
								</tr>
							</tbody>
						</table>
						<div class="dis-fl fl-rwnw jsc-ct alit-flend">
							<a href="#" class="btn btn-drk">полная таблица</a>
						</div>
					</div>
				</div>
			</section>
			<!-- ********* -->
			<!-- 2ND BLOCK -->
			<section class="sect sect-news-media-social dis-fl fl-rwwr jsc-spbet alit-ct">
				<div class="content dis-grid">
					<div>
						<section class="news dis-fl fl-colnw jsc-flst alit-flst">
							<div class="title dis-fl fl-rwwr jsc-flst alit-flend">
								<h2>Новости</h2>
								<a href="#" class="lnk">все новости</a>
							</div>
							<div class="wall dis-grid">
								<figure class="post max">
									<a href="#">
										<div class="img">
											<img src="img/news-post-main.jpg" alt="">
										</div>
										<figcaption class="dis-fl fl-colnw jsc-flend">
											<h3>Первый тур Зимнего Первенства для U-17</h3>
											<p class="date">09.12.2019</p>
										</figcaption>
									</a>
								</figure>
								<figure class="post min">
									<a href="#">
										<div class="img">
											<img src="img/news-post.jpg" alt="">
										</div>
										<figcaption class="dis-fl fl-colnw jsc-flend">
											<h3>Рубрика «СМИ о нас». Корреспондент «Спорт день за днём» побывал на закрытии...</h3>
											<p class="date">07.12.2019</p>
										</figcaption>
									</a>
								</figure>
								<figure class="post min">
									<a href="#">
										<div class="img">
											<img src="img/news-post.jpg" alt="">
										</div>
										<figcaption class="dis-fl fl-colnw jsc-flend">
											<h3>Рубрика «СМИ о нас». Корреспондент «Спорт день за днём» побывал на закрытии...</h3>
											<p class="date">07.12.2019</p>
										</figcaption>
									</a>
								</figure>
								<figure class="post min">
									<a href="#">
										<div class="img">
											<img src="img/news-post.jpg" alt="">
										</div>
										<figcaption class="dis-fl fl-colnw jsc-flend">
											<h3>Рубрика «СМИ о нас». Корреспондент «Спорт день за днём» побывал на закрытии...</h3>
											<p class="date">07.12.2019</p>
										</figcaption>
									</a>
								</figure>
								<figure class="post min">
									<a href="#">
										<div class="img">
											<img src="img/news-post.jpg" alt="">
										</div>
										<figcaption class="dis-fl fl-colnw jsc-flend">
											<h3>Рубрика «СМИ о нас». Корреспондент «Спорт день за днём» побывал на закрытии...</h3>
											<p class="date">07.12.2019</p>
										</figcaption>
									</a>
								</figure>
								<figure class="post min">
									<a href="#">
										<div class="img">
											<img src="img/news-post.jpg" alt="">
										</div>
										<figcaption class="dis-fl fl-colnw jsc-flend">
											<h3>Рубрика «СМИ о нас». Корреспондент «Спорт день за днём» побывал на закрытии...</h3>
											<p class="date">07.12.2019</p>
										</figcaption>
									</a>
								</figure>
							</div>
						</section>
						<section class="media dis-fl fl-colnw jsc-flst alit-flst">
							<div class="title dis-fl fl-rwwr jsc-flst alit-flend">
								<h2>медиа</h2>
								<a href="" class="lnk">все медиа</a>
							</div>
							<div class="wall dis-grid">
								<figure class="post min">
									<a href="#">
										<div class="img">
											<img src="img/media-post.jpg" alt="">
										</div>
										<figcaption class="dis-fl fl-colnw jsc-spbet">
											<div class="icons">
												<i class="icon photo">
													<span>103</span>
												</i>
											</div>
											<h3>Церемония награждения сезона 2019 в Сибур Арене</h3>
										</figcaption>
									</a>
								</figure>
								<figure class="post min">
									<a href="#">
										<div class="img">
											<img src="img/media-post.jpg" alt="">
										</div>
										<figcaption class="dis-fl fl-colnw jsc-spbet">
											<div class="icons">
												<i class="icon video"></i>
											</div>
											<h3>Церемония награждения сезона 2019 в Сибур Арене</h3>
										</figcaption>
									</a>
								</figure>
								<figure class="post min">
									<a href="#">
										<div class="img">
											<img src="img/media-post.jpg" alt="">
										</div>
										<figcaption class="dis-fl fl-colnw jsc-spbet">
											<div class="icons">
												<i class="icon video"></i>
											</div>
											<h3>Церемония награждения сезона 2019 в Сибур Арене</h3>
										</figcaption>
									</a>
								</figure>
								<figure class="post min">
									<a href="#">
										<div class="img">
											<img src="img/media-post.jpg" alt="">
										</div>
										<figcaption class="dis-fl fl-colnw jsc-spbet">
											<div class="icons">
												<i class="icon video"></i>
											</div>
											<h3>Церемония награждения сезона 2019 в Сибур Арене</h3>
										</figcaption>
									</a>
								</figure>
								<figure class="post min">
									<a href="#">
										<div class="img">
											<img src="img/media-post.jpg" alt="">
										</div>
										<figcaption class="dis-fl fl-colnw jsc-spbet">
											<div class="icons">
												<i class="icon photo">
													<span>27</span>
												</i>
											</div>
											<h3>Церемония награждения сезона 2019 в Сибур Арене</h3>
										</figcaption>
									</a>
								</figure>
								<figure class="post min">
									<a href="#">
										<div class="img">
											<img src="img/media-post.jpg" alt="">
										</div>
										<figcaption class="dis-fl fl-colnw jsc-spbet">
											<div class="icons">
												<i class="icon photo">
													<span>16</span>
												</i>
											</div>
											<h3>Церемония награждения сезона 2019 в Сибур Арене</h3>
										</figcaption>
									</a>
								</figure>
							</div>
						</section>
					</div>
					<aside class="insta dis-fl fl-colnw jsc-flst alit-flst">
						<div class="title">
							<h2>мы в instagram</h2>
						</div>
						<!-- delete this img before parsing instagram posts! -->
						<div class="insta-content">
							<img src="img/insta-posts.jpg" alt="">
						</div>
						<a href="#" class="btn btn-drk">показать ещё</a>
					</aside>
				</div>
			</section>
			<!-- ********* -->
			<!-- 3RD BLOCK -->
			<section class="sect sect-trophy">
				<div class="content dis-fl fl-colnw jsc-flst alit-flst">
					<div class="title dis-fl fl-rwwr jsc-flst alit-flend">
						<h2>Кубки</h2>
						<a href="" class="lnk lnk-light">подробнее</a>
					</div>
					<!-- slider 4 -->
					<div class="slider" id="slider-trophy">
						<div class="arrows dis-fl fl-rwnw jsc-spbet alit-ct" id="arrows_4"></div>
						<div class="carousel-cell">
							<figure class="trophy">
								<a href="#">
									<div class="img dis-fl fl-colnw jsc-flend alit-ct">
										<img src="img/trophy-full-1.png" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-spbet">
										<p class="date">2019</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="trophy">
								<a href="#">
									<div class="img dis-fl fl-colnw jsc-flend alit-ct">
										<img src="img/trophy-full-2.png" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-spbet">
										<p class="date">2019</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="trophy">
								<a href="#">
									<div class="img dis-fl fl-colnw jsc-flend alit-ct">
										<img src="img/trophy-full-3.png" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-spbet">
										<p class="date">2019</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="trophy">
								<a href="#">
									<div class="img dis-fl fl-colnw jsc-flend alit-ct">
										<img src="img/trophy-full-4.png" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-spbet">
										<p class="date">2019</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="trophy">
								<a href="#">
									<div class="img dis-fl fl-colnw jsc-flend alit-ct">
										<img src="img/trophy-full-5.png" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-spbet">
										<p class="date">2019</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="trophy">
								<a href="#">
									<div class="img dis-fl fl-colnw jsc-flend alit-ct">
										<img src="img/trophy-full-6.png" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-spbet">
										<p class="date">2019</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="trophy">
								<a href="#">
									<div class="img dis-fl fl-colnw jsc-flend alit-ct">
										<img src="img/trophy-full-7.png" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-spbet">
										<p class="date">2019</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="trophy">
								<a href="#">
									<div class="img dis-fl fl-colnw jsc-flend alit-ct">
										<img src="img/trophy-full-8.png" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-spbet">
										<p class="date">2019</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="trophy">
								<a href="#">
									<div class="img dis-fl fl-colnw jsc-flend alit-ct">
										<img src="img/trophy-full-1.png" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-spbet">
										<p class="date">2019</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="trophy">
								<a href="#">
									<div class="img dis-fl fl-colnw jsc-flend alit-ct">
										<img src="img/trophy-full-2.png" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-spbet">
										<p class="date">2019</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="trophy">
								<a href="#">
									<div class="img dis-fl fl-colnw jsc-flend alit-ct">
										<img src="img/trophy-full-3.png" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-spbet">
										<p class="date">2019</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="trophy">
								<a href="#">
									<div class="img dis-fl fl-colnw jsc-flend alit-ct">
										<img src="img/trophy-full-4.png" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-spbet">
										<p class="date">2019</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="trophy">
								<a href="#">
									<div class="img dis-fl fl-colnw jsc-flend alit-ct">
										<img src="img/trophy-full-5.png" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-spbet">
										<p class="date">2019</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="trophy">
								<a href="#">
									<div class="img dis-fl fl-colnw jsc-flend alit-ct">
										<img src="img/trophy-full-6.png" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-spbet">
										<p class="date">2019</p>
									</figcaption>
								</a>
							</figure>
						</div>
						<div class="carousel-cell">
							<figure class="trophy">
								<a href="#">
									<div class="img dis-fl fl-colnw jsc-flend alit-ct">
										<img src="img/trophy-full-7.png" alt="">
									</div>
									<figcaption class="dis-fl fl-colnw jsc-spbet">
										<p class="date">2019</p>
									</figcaption>
								</a>
							</figure>
						</div>
					</div>
				</div>
			</section>
			<!-- ********* -->
			<!-- 4TH BLOCK -->
			<section class="sect sect-sponsors">
				<div class="content dis-fl fl-colnw jsc-flst alit-flst">
					<div class="title dis-fl fl-rwwr jsc-flst alit-flend">
						<h2>партнёры и спонсоры</h2>
						<a href="" class="lnk">подробнее</a>
					</div>
					<div class="spons-wall dis-fl fl-rwwr jsc-ct alit-strt">
						<a href="#" class="card dis-fl fl-colnw jsc-flst alit-ct">
							<img src="img/sponsor-logo.png" alt="">
							<p class="desc">СЗРЦ Концерна ВКО Алмаз-Антей</p>
						</a>
						<a href="#" class="card dis-fl fl-colnw jsc-flst alit-ct">
							<img src="img/sponsor-logo-2.png" alt="">
							<p class="desc">АО Завод Радиотехнического Оборудование</p>
						</a>
						<a href="#" class="card dis-fl fl-colnw jsc-flst alit-ct">
							<img src="img/sponsor-logo.png" alt="">
							<p class="desc">Конструкторское бюро специального машиностроения</p>
						</a>
						<a href="#" class="card dis-fl fl-colnw jsc-flst alit-ct">
							<img src="img/sponsor-logo-2.png" alt="">
							<p class="desc">АО «Ордена Трудового Красного Знамени Всероссийский научно-исследовательский институт радиоаппаратуры»</p>
						</a>
						<a href="#" class="card dis-fl fl-colnw jsc-flst alit-ct">
							<img src="img/sponsor-logo.png" alt="">
							<p class="desc">АО «ГОЗ Обуховский завод»</p>
						</a>
						<a href="#" class="card dis-fl fl-colnw jsc-flst alit-ct">
							<img src="img/sponsor-logo-2.png" alt="">
							<p class="desc">АО «Российский институт радионавигации и времени»</p>
						</a>
						<a href="#" class="card dis-fl fl-colnw jsc-flst alit-ct">
							<img src="img/sponsor-logo.png" alt="">
							<p class="desc">ОАО «Научно-производственное предприятие «Пружинный Центр»</p>
						</a>
					</div>
				</div>
			</section>
			<!-- ********* -->
			<!-- FOOTER -->
			<footer>
				<div class="dis-fl fl-colnw jsc-flst alit-flst">
					<div class="top">
						<div class="container dis-fl fl-rwwr jsc-spbet alit-ct">
							<p class="copyright">© 2019. ФК «Алмаз-Антей»</p>
							<a class="tel" href="tel:8123639760">(812) 363-97-60</a>
							<a class="mailto" href="mailto:info@fcalmaz-antey.ru">info@fcalmaz-antey.ru</a>
						</div>
					</div>
					<div class="bot">
						<div class="container dis-fl fl-rwwr jsc-flend alit-ct">
							<div class="sogl dis-fl fl-rwwr jsc-ct alit-ct">
								<a href="#">пользовательское соглашение</a>
								<a href="#">Соглашение о конфиденциальности</a>
							</div>
							<ul class="social">
								<li><a href="#">
									<i class="fab fa-vk"></i>
								</a></li>
								<li><a href="#">
									<i class="fab fa-youtube"></i>
								</a></li>
								<li><a href="#">
									<i class="fab fa-instagram"></i>
								</a></li>
							</ul>
						</div>
					</div>
				</div>
			</footer>
		</main>
		<!-- styles -->
		<link rel="stylesheet" href="css/fontawesome.min.css">
		<link rel="stylesheet" href="css/solid.min.css">
		<link rel="stylesheet" href="css/regular.min.css">
		<link rel="stylesheet" href="css/brands.min.css">
		<link rel="stylesheet" href="css/jquery.fancybox.min.css">
		<link rel="stylesheet" href="css/slick.css">
		<link rel="stylesheet" href="css/select2.css">
		<!-- scripts -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<script src="js/jquery.maskedinput.js"></script>
		<script src="js/jquery.fancybox.min.js"></script>
		<script src="js/doubletaptogo.js"></script>
		<script src="js/select2.min.js"></script>
		<script src="js/slick.min.js"></script>
		<script src="js/custom.js"></script>
	</body>
</html>