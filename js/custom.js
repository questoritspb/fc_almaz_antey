// new
/*
 * HTML5 Shiv v3.7.0 | @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed
 */
(function(l,f){function m(){var a=e.elements;return"string"==typeof a?a.split(" "):a}function i(a){var b=n[a[o]];b||(b={},h++,a[o]=h,n[h]=b);return b}function p(a,b,c){b||(b=f);if(g)return b.createElement(a);c||(c=i(b));b=c.cache[a]?c.cache[a].cloneNode():r.test(a)?(c.cache[a]=c.createElem(a)).cloneNode():c.createElem(a);return b.canHaveChildren&&!s.test(a)?c.frag.appendChild(b):b}function t(a,b){if(!b.cache)b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag();
a.createElement=function(c){return!e.shivMethods?b.createElem(c):p(c,a,b)};a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+m().join().replace(/[\w\-]+/g,function(a){b.createElem(a);b.frag.createElement(a);return'c("'+a+'")'})+");return n}")(e,b.frag)}function q(a){a||(a=f);var b=i(a);if(e.shivCSS&&!j&&!b.hasCSS){var c,d=a;c=d.createElement("p");d=d.getElementsByTagName("head")[0]||d.documentElement;c.innerHTML="x<style>article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}</style>";
c=d.insertBefore(c.lastChild,d.firstChild);b.hasCSS=!!c}g||t(a,b);return a}var k=l.html5||{},s=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,r=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,j,o="_html5shiv",h=0,n={},g;(function(){try{var a=f.createElement("a");a.innerHTML="<xyz></xyz>";j="hidden"in a;var b;if(!(b=1==a.childNodes.length)){f.createElement("a");var c=f.createDocumentFragment();b="undefined"==typeof c.cloneNode||
"undefined"==typeof c.createDocumentFragment||"undefined"==typeof c.createElement}g=b}catch(d){g=j=!0}})();var e={elements:k.elements||"abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",version:"3.7.0",shivCSS:!1!==k.shivCSS,supportsUnknownElements:g,shivMethods:!1!==k.shivMethods,type:"default",shivDocument:q,createElement:p,createDocumentFragment:function(a,b){a||(a=f);
if(g)return a.createDocumentFragment();for(var b=b||i(a),c=b.frag.cloneNode(),d=0,e=m(),h=e.length;d<h;d++)c.createElement(e[d]);return c}};l.html5=e;q(f)})(this,document);

/*-------------------*/
/*-------------------*/
// custom scripts
/*-------------------*/
/*-------------------*/
// toggle hamburger & collapse nav
$('.menu-toggle').click(function() {
	$('.site-nav').toggleClass('site-nav--open');
	$(this).toggleClass('open');
});
/*-------------------*/
/*-------------------*/
// collapse header
/*$(function(){
	$(window).scroll(function() {
		if($(window).scrollTop() >= 200) {
			$('header').addClass('scrolled');
		}
		else {
			$('header').removeClass('scrolled');
		}
	});
});*/
/*-------------------*/
/*-------------------*/
// double tap to click in mobile menu
$( 'nav li:has(ul)' ).doubleTapToGo();
/*-------------------*/
/*-------------------*/
// 2nd menu on click/hover
$(window).load(function() {
	var viewportWidth = $(window).width();
	if (viewportWidth < 821) {
		$('a.dropdown').click(function() {
			$(this).toggleClass('active');
			$(this).next().toggleClass('active');
		});
	} else {
		$('li').on('mouseenter', function() {
			$(this).find('ul.dropdown').addClass('active');
		});
		$('li').on('mouseleave', function() {
			$(this).find('ul.dropdown').removeClass('active');
		});
	}
});
/*-------------------*/
/*-------------------*/
// sliders
$(document).ready(function(){
	$('#slider-main').slick({
		slide: '.carousel-cell',
		centerMode: false,
		infinite: true,
		appendArrows: $('#arrows'),
		dots: true,
		prevArrow: '<button type="button" class="slick-prev"></button>',
		nextArrow: '<button type="button" class="slick-next"></button>',
		responsive: [
			{
				breakpoint: 430,
				settings: {
					arrows: false
				}
			}
		]
	});
	$('#slider-match_center').slick({
		slide: '.carousel-cell',
		centerMode: false,
		infinite: true,
		appendArrows: $('#arrows_2'),
		prevArrow: '<button type="button" class="slick-prev"></button>',
		nextArrow: '<button type="button" class="slick-next"></button>',
		responsive: [
			{
				breakpoint: 430,
				settings: {
					arrows: false
				}
			}
		]
	});
	$('#slider-calendar').slick({
		slide: '.carousel-cell',
		centerMode: false,
		infinite: true,
		appendArrows: $('#arrows_3'),
		prevArrow: '<button type="button" class="slick-prev"></button>',
		nextArrow: '<button type="button" class="slick-next"></button>'
	});
	$('#slider-trophy').slick({
		slide: '.carousel-cell',
		centerMode: false,
		infinite: false,
		slidesToShow: 8,
		appendArrows: $('#arrows_4'),
		prevArrow: '<button type="button" class="slick-prev"></button>',
		nextArrow: '<button type="button" class="slick-next"></button>',
		responsive: [
			{
				breakpoint: 1300,
				settings: {
					slidesToShow: 6
				}
			},
			{
				breakpoint: 1034,
				settings: {
					slidesToShow: 5
				}
			},
			{
				breakpoint: 870,
				settings: {
					slidesToShow: 4
				}
			},
			{
				breakpoint: 430,
				settings: {
					slidesToShow: 2
				}
			}
		]
	});
});
/*-------------------*/
/*-------------------*/
// custom-select
$(document).ready(function() {
	$('.select').select2({
		minimumResultsForSearch: Infinity
	});
});
/*-------------------*/
/*-------------------*/
// instagram height
$(document).ready(function() {
	var i = $('section.news').height();
	var e = $('section.media').height();
	var viewportWidth = $(window).width();
	if (viewportWidth > 1386) {
		var j = i+e-12;
	} else if (viewportWidth < 1386) {
		var j = i+e-6;
	}
	$('.insta-content').css({
		height: j
	});
});
$(window).resize(function() {
	var i = $('section.news').height();
	var e = $('section.media').height();
	var viewportWidth = $(window).width();
	if (viewportWidth > 1386) {
		var j = i+e-12;
	} else if (viewportWidth < 1386) {
		var j = i+e-6;
	}
	$('.insta-content').css({
		height: j
	});
});
/*-------------------*/
/*-------------------*/
// input file dropbox
/*function dropHandler(ev) {
	// console.log('File(s) dropped');
	// prevent default behavior (Prevent file from being opened)
	ev.preventDefault();
	if (ev.dataTransfer.items) {
		// use DataTransferItemList interface to access the file(s)
		for (var i = 0; i < ev.dataTransfer.items.length; i++) {
			// If dropped items aren't files, reject them
			if (ev.dataTransfer.items[i].kind === 'file') {
				var file = ev.dataTransfer.items[i].getAsFile();
				// console.log('... file[' + i + '].name = ' + file.name);
				$('#dropbox').text(file.name);
				$('#dropbox').removeClass('error');
				$('#dropbox').addClass('confirm');
			}
		}
	} else {
		// use DataTransfer interface to access the file(s)
		for (var i = 0; i < ev.dataTransfer.files.length; i++) {
			// console.log('... file[' + i + '].name = ' + ev.dataTransfer.files[i].name);
			$('#dropbox').removeClass('confirm');
			$('#dropbox').addClass('error');
		}
	}
}
function dragOverHandler(ev) {
	// console.log('File(s) in drop zone');
	// prevent default behavior (Prevent file from being opened)
	ev.preventDefault();
}*/
$('#file').change(function() {
	var filename = $('input[type=file]').val().split('\\').pop();
	if ($(this).val() != '') {
		$(this).next().text(filename);
		$('#dropbox').removeClass('error');
		$('#dropbox').addClass('confirm');
	} else {
		$(this).next().text('Перетащите или прикрепите файл');
		$('#dropbox').removeClass('confirm');
		$('#dropbox').addClass('error');
	}
});
/*-------------------*/
/*-------------------*/
//  match-center events
$(document).ready(function() {
	var time = $('.time').attr('data-time');
	$('.events .item').each(function() {
		var pos = $(this).attr('data-time');
		var pos_new = 100 / time * pos;
		$(this).css({
			left: pos_new + '%'
		});
	});
});
$(document).ready(function() {
	var elements = $('.events-wall .event');
	var target = $('.events-wall');
	var items = [];
	var dtime = [];
	var cont = [];
	$(elements).each(function(key, value) {
		dtime = +$(this).attr('data-time');
		cont = $(this).get(0).outerHTML;
		var skob = {data_time: dtime, content: cont};
		items.push(skob);
		items.sort((a, b) => a.data_time - b.data_time);
	});
	elements.remove();
	$(items).each(function(index, el) {
		$(el).map(function(key, value) {
			target.append(value.content);
		});
	});
});
/*-------------------*/
/*-------------------*/
// match-center stat
$(document).ready(function() {
	$('.stat-wall .stat').each(function(index, el) {
		var res1 = $(this).attr('data_res1');
		var res2 = $(this).attr('data_res2');
		var resall = +res1 + +res2;
		var w_new = 100 / resall * res1;
		$(this).find('.line .res').animate({width: w_new + '%'}, 1500);
	});
});
$('ul.tabs li a').click(function() {
	$('.stat-wall .stat').find('.line .res').css({
		width: '0%'
	});
});
$('ul.tabs li a[href="#stat"]').click(function() {
	$('.stat-wall .stat').each(function(index, el) {
		var res1 = $(this).attr('data_res1');
		var res2 = $(this).attr('data_res2');
		var resall = +res1 + +res2;
		var w_new = 100 / resall * res1;
		console.log(w_new);
		$(this).find('.line .res').animate({width: w_new + '%'}, 1500);
	});
});
/*-------------------*/
/*-------------------*/
/*$(document).ready(function() {
	$('.stat-wall .stat').each(function(index, el) {
		var res1 = $(this).attr('data_res1');
		var res2 = $(this).attr('data_res2');
		var resall = +res1 + +res2;
		var w_new = 100 / resall * res1;
		$(this).find('.line .res').css({
			width: w_new + '%'
		});
	});
});*/