// catalog
(function($, window, document, undefined) {
    'use strict';
    // init cubeportfolio
    $('#js-grid-lightbox-gallery').cubeportfolio({
        filters: '#js-filters-lightbox-gallery',
        loadMore: '#js-loadMore-lightbox-gallery',
        loadMoreAction: 'click',
        layoutMode: 'grid',
        mediaQueries: [{
            width: 1500,
            cols: 4,
        }, {
            width: 1100,
            cols: 3,
        }, {
            width: 800,
            cols: 3,
        }, {
            width: 480,
            cols: 2,
            options: {
                caption: '',
            }
        }],
        defaultFilter: '*',
        animationType: 'flipOutDelay',
        gapHorizontal: 10,
        gapVertical: 10,
        gridAdjustment: 'responsive',
        caption: 'default',
        displayType: 'sequentially',
        displayTypeSpeed: 100,
        // lightbox
        lightboxDelegate: '.cbp-lightbox',
        lightboxGallery: true,
        lightboxTitleSrc: 'data-title',
        lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',
        // singlePageInline
        singlePageInlineDelegate: '.cbp-singlePageInline',
        singlePageInlinePosition: 'below',
        singlePageInlineInFocus: true,
        singlePageInlineCallback: function(url, element) {
            // to update singlePageInline content use the following method: this.updateSinglePageInline(yourContent)
            var t = this;
            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 30000
                })
                .done(function(result) {

                    t.updateSinglePageInline(result);

                })
                .fail(function() {
                    t.updateSinglePageInline('AJAX Error! Please refresh the page!');
                });
        },
        plugins: {
			loadMore: {
				element: '#js-loadMore-lightbox-gallery',
				action: 'click',
				loadItems: 4,
			},
		},
    });
})(jQuery, window, document);

// about-gallery
(function($, window, document, undefined) {
    'use strict';
    // init cubeportfolio
    $('#js-grid-about,#js-grid-reviews').cubeportfolio({
        layoutMode: 'slider',
        drag: true,
        auto: true,
        autoTimeout: 3000,
        autoPauseOnHover: true,
        showNavigation: false,
        showPagination: true,
        rewindNav: true,
        scrollByPage: false,
        gridAdjustment: 'responsive',
        mediaQueries: [{
            width: 1500,
            cols: 6,
        }, {
            width: 1100,
            cols: 5,
        }, {
            width: 800,
            cols: 4,
        }, {
            width: 480,
            cols: 2,
        }],
        gapHorizontal: 0,
        gapVertical: 5,
        caption: 'opacity',
        displayType: 'fadeIn',
        displayTypeSpeed: 100,
    });
})(jQuery, window, document);

// clients
(function($, window, document, undefined) {
    'use strict';
    // init cubeportfolio
    var singlePage = $('#js-singlePage-container').children('div');
    $('#js-grid-slider-clients').cubeportfolio({
        layoutMode: 'slider',
        drag: true,
        auto: false,
        autoTimeout: 5000,
        autoPauseOnHover: true,
        showNavigation: true,
        showPagination: false,
        rewindNav: false,
        scrollByPage: false,
        gridAdjustment: 'responsive',
        mediaQueries: [{
            width: 1500,
            cols: 5,
        }, {
            width: 1100,
            cols: 4,
        }, {
            width: 800,
            cols: 3,
        }, {
            width: 480,
            cols: 2,
            options: {
                caption: '',
                gapVertical: 10,
            }
        }],
        gapHorizontal: 0,
        gapVertical: 25,
        caption: 'default',
        displayType: 'fadeIn',
        displayTypeSpeed: 100,
    });
})(jQuery, window, document);

// reviews
(function($, window, document, undefined) {
    'use strict';
    // init cubeportfolio
    $('#js-grid-reviews-wall').cubeportfolio({
        filters: '',
        loadMore: '#js-loadMore-reviews-wall',
        loadMoreAction: 'click',
        layoutMode: 'grid',
        mediaQueries: [{
            width: 1500,
            cols: 2,
        }, {
            width: 1100,
            cols: 2,
        }, {
            width: 800,
            cols: 2,
        }, {
            width: 480,
            cols: 1,
            options: {
                caption: '',
            }
        }],
        defaultFilter: '*',
        animationType: 'flipOutDelay',
        gapHorizontal: 20,
        gapVertical: 10,
        gridAdjustment: 'responsive',
        caption: 'default',
        displayType: 'sequentially',
        displayTypeSpeed: 100,
        plugins: {
			loadMore: {
				element: '#js-loadMore-reviews-wall',
				action: 'click',
				loadItems: 4,
			},
		},
    });
})(jQuery, window, document);